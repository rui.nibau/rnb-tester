Title:      rnb-tester
Date:       2019-02-09
Updated:    2021-04-26
Cats:       web
Tags:       javascript, tests, debug
State:      completed
Technos:    javascript
Source:     https://framagit.org/rui.nibau/rnb-tester
Issues:     https://framagit.org/rui.nibau/rnb-tester/-/issues
Intro: Un utilitaire extrêmement simple de tests unitaires en javascript développé il y a des années et remis au gout du jour.

°°msg msg-info°°Ce petit module n'a plus vraiment de raison d'être suite à une nouvelle manière de voir [les tests en javascript](/n/2021-04-26).

## Présentation

Rnb-tester est un outil qui n'est absolument pas destiné à produire des tests unitaires d'envergures ou de couvrir l'ensemble des besoins que ce type d'activité exige. C'est un truc écrit en une heure il y a presque dix ans qui répond à un besoin basique : **faire rapidement et sans fioritures** des tests unitaires de base pour valider un code et s'éviter des régressions bêtes. D'autres outils existent pour des validations plus complexes.

Le code est ici mis à jour en « javascript moderne » sous forme de module. Il fait aussi parti d'une réécriture progressive de [ma librairie javascript](/p/rnb-js/) afin d'obtenir des modules indépendants les uns des autres.

## Exemple

----
°°stx-js°°
    import Test from './path/to/rnb/tester/Test';
    
    const t = new Test('New test suite', 'html');
    t.testing('Boolean');
    t.equal(!false, true, 'True Equals !false');
    t.testing('String - trim');
    t.equal('  Hello World  '.trim(), 'Hello World', 'Trim spaces');
----
Exemple d'utilisation — oui, les tests sont débiles.

°°pic pos-centre sz-full°°
----
![rendu de la suite de test](/lab/rnb-tester/pics/example.jpg)
----
Exemple de rendu HTML d'une suite de tests.

## Historique

..include::./changelog.md

## Licence

..include::./licence.md

