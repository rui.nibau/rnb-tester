/**
 *  Html logger : write pass and fail asserts in a bullet list.
 */
class HtmlLogger {
    constructor () {
        this.element = document.createElement('div');
        this.element.id = 'log';
        this.element.classList.add('jstester');
        document.body.appendChild(this.element);
    }

    /**
     * Remove log element from document
     */
    dispose () {
        this.element.parentNode.removeChild(this.element);
        this.element = null;
    }

    /**
     * Clear all logs
     */
    clear () {
        this.element.innerHTML = '';
    }

    /**
     * @param {String} title 
     * @param {Number} level 
     */
    title (title, level) {
        const item = document.createElement(`h${level ? level : '1'}`);
        item.textContent = title;
        this.element.appendChild(item);
    }

    /**
     * Append a log message
     * @param {String} message Message to log
     */
    log (message) {
        const item = document.createElement('p');
        item.className = 'log';
        item.textContent = message;
        this.element.appendChild(item);
    }

    /**
     * Append an passed assertion message
     * @param {String} message Description message
     */
    pass (message) {
        const item = document.createElement('p');
        item.className = 'pass';
        item.textContent = message;
        this.element.appendChild(item);
    }

    /**
     * Append a fail assertion message
     * @param {any} actual
     * @param {any} expected
     * @param {string} message
     */
    fail (actual, expected, message) {
        const item = document.createElement('p');
        item.className = 'fail';
        item.textContent = message;
        this.element.append(item, this.detail('actual', actual), this.detail('expected', expected));
    }

    detail (name, content) {
        const dl = document.createElement('dl'),
            dt = document.createElement('dt'),
            dd = document.createElement('dd'),
            pre = document.createElement('pre');
        dt.textContent = name;
        pre.textContent = content;
        dd.appendChild(pre);
        dl.append(dt, dd);
        return dl;
    }
}

class ConsoleLogger {
    constructor () {}
    /**
     * @param {String} title Title
     * @param {Number} [level] Level
     */
    title  (title, level) {
        console.log(title.padStart(level ? level : 1, '#'));
    }
    log (message) {
        console.log(message); 
    }
    pass (message) {
        console.log(`ok: ${message}`); 
    }
    fail (actual, expected, message) {
        console.warn(`ko: ${message}`); 
        console.group();
        console.info('Actual:');
        console.log(actual);
        console.info('Expected:');
        console.log(expected);
        console.groupEnd();
    }
    clear () {}
    dispose () {}
}

/**
 * Classe de test unitaire
 */
export default class Test {
    
    /**
     * @param {String} title Test suite title
     * @param {String} [loggerType='html'|'console'] Type de log souhaité (console par défaut)
     */
    constructor (title, loggerType) {
        this.logger = loggerType === 'html' ? new HtmlLogger() : new ConsoleLogger();
        this.logger.title(title, 1);
    }
    
    /**
     * Clear logger
     */
    clear () {
        this.logger.clear();
    }
    
    /**
     * Dispose
     */
    dispose () {
        this.logger.dispose();
        this.logger = null;
    }
    
    /**
     * Define the title of the current testing actions
     * @param {String} title Test title
     */
    testing (title) {
        this.logger.title(title, 2);
    }

    /**
     * @param {any} actual Actual value
     * @param {any} expected Expected value
     * @param {String} message
     */
    equal (actual, expected, message) {
        if (actual === expected) {
            this.logger.pass(message);
        } else {
            this.logger.fail(actual, expected, message);
        }
    }

    /**
     * @param {any} actual Actual value
     * @param {String} message
     */
    null (actual, message) {
        if (actual === null) {
            this.logger.pass(message);
        } else {
            this.logger.fail(actual, null, message);
        }
    }

    /**
     * @param {any} actual Actual value
     * @param {String} message
     */
    true (actual, message) {
        if (actual === true) {
            this.logger.pass(message);
        } else {
            this.logger.fail(actual, true, message);
        }
    }
}
