
const isNode = globalThis.document ? false : true;

/**
 * Shortcut for the console.assert function, testing actual === expected.
 * 
 * @param {TestDefinition} test 
 */
export const assert = test => {
    console.assert(test.actual === test.expected, isNode ? JSON.stringify(test) : test);
};

/**
 * Run a bunch of tests and output on console.
 * 
 * @param {String} title 
 * @param {Object.<string,Function>} tests 
 */
export const runTests = async (title, tests) => {
    console.group(title);
    for (const [title, test] of Object.entries(tests)) {
        console.group(title);
        await test();
        console.groupEnd();
    }
    console.groupEnd();
};
